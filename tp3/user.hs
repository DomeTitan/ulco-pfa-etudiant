
data User = User 
    { _nom :: String
    , _prenom :: String
    , _age :: Int
    }

showUser :: User -> String
showUser u = (_nom u) ++ " " ++ (_prenom u) ++ " " ++ show (_age u)

incAge :: User -> User
incAge u = u {_age = 1 + _age u}

main :: IO ()
main = do
    let u1 = User "doe" "john" 42
        u2 = incAge u1
    putStrLn $ showUser u1
    putStrLn $ showUser u2

