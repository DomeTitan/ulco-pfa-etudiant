import Data.List (foldl')

data Tree = Leaf | Node Int Tree Tree

insererAbr :: Int -> Tree -> Tree
insererAbr x Leaf = Node x Leaf Leaf
insererAbr x (Node y left right)
    | x < y = Node y (insererAbr x left) right
    | otherwise = Node y (insererAbr x right) left

listToAbr  :: [Int] -> Tree
listToAbr = foldr insererAbr Leaf

abrToList  :: Tree -> [Int]
abrToList Leaf = []
abrToList (Node x l r) = abrToList l ++ [x] ++ abrToList r

main :: IO ()
main = do
    let t1 = Leaf
        t2 = Node 12 (Node 3 Leaf(Node 7 Leaf Leaf))
                    (Node 13 (Node 12 Leaf Leaf) (Node 42 Leaf Leaf))
        t3 = insererAbr 37 t1
        t4 = insererAbr 37 t2
        t5 = listToAbr [12, 3, 7, 13, 42, 12]
        l6 = abrToList t5
    print l6

