
data List a = Nil | Cons a (List a)

sumList :: List Int -> Int
sumList Nil = 0
sumList (Cons x xs) = x + sumList xs

flatList :: List String -> String
flatList Nil = ""
flatList (Cons x xs) = x ++ (flatList xs)

toHaskell :: List a -> [a]
toHaskell Nil = []
toHaskell (Cons x xs) = x : toHaskell xs

fromHaskell :: [a] -> List a
--fromHaskell [] = Nil
--fromHaskell (x:xs) = Cons x (fromHaskell xs)
fromHaskell = foldr Cons Nil

myShowList :: Show a => List a -> String
--myShowList Nil = ""
--myShowList (Cons x xs) = show x ++ " " ++ myShowList xs
myShowList = unwords . map show . toHaskell

main :: IO ()
main = do
    let l1 = Nil
        l2 = Cons 2 Nil
        l3 = Cons 1 (Cons 2 Nil)
    print $ sumList l1
    print $ sumList l2
    print $ sumList l3
    print $ toHaskell l3
    print $ toHaskell $ fromHaskell [1..4::Int]
    putStrLn $ myShowList l3
