{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Model where

import Data.Text (Text)
import Database.SQLite.Simple
import GHC.Generics (Generic)

data Discussion = Discussion
    { discussion_id :: Int
    , discussion_sujet :: Text
    } deriving (Generic, Show)

instance FromRow Discussion where
    fromRow = Discussion <$> field <*> field

selectAllDiscusions :: Connection -> IO[Discussion]
selectAllDiscusions conn = query_ conn
    "SELECT * FROM discussion"
