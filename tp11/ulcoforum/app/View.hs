{-# LANGUAGE OverloadedStrings #-}

module View where

import Lucid
import Model

viewPage :: [Discussion] -> Html ()
viewPage discussions = do
    doctype_
    html_ $ do
        body_ $ do
            ul_ $ mapM_ viewDiscussion discussions

viewDisscusion :: Discussion -> Html()
viewDisscusion (Discussion discussion_id discussion_sujet) = li_ $ toHtml discussion_sujet