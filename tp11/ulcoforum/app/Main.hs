{-# LANGUAGE OverloadedStrings #-}

import View
import Model

import Lucid
import Web.Scotty
import Control.Monad.IO.Class (liftIO)
import Database.SQLite.Simple

main :: IO ()
main = scotty 3000 $
    get "/" $ do
        discusions <- liftIO $ withConnection "bdd.db" selectAllDiscusions
        html $ renderText $ viewPage discusions
