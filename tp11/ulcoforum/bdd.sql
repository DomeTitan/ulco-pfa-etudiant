-- create database:
-- sqlite3 bdd.db < bdd.sql


CREATE TABLE discussion (
  discussion_id INTEGER PRIMARY KEY AUTOINCREMENT,
  discussion_sujet TEXT
);

CREATE TABLE message (
  message_id INTEGER PRIMARY KEY AUTOINCREMENT,
  message_auteur TEXT,
  message_text TEXT,
  message_discussion INTEGER,
  FOREIGN KEY(message_discussion) REFERENCES discussion(discussion_id)
);

INSERT INTO discussion VALUES(1, "Vacances 2020-2021");
INSERT INTO discussion VALUES(2, "Master info nouveau programme");

INSERT INTO message VALUES(1, 1, 'Jhon', "Pas de vacances cette année");
INSERT INTO message VALUES(2, 1, 'Toto', "Youpie!");
INSERT INTO message VALUES(3, 2, 'Toto', "Tous les cours passent en Haskell");
