{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Model where

import GHC.Generics
import Data.Aeson (ToJSON)

data Rider = Rider
    {_title :: String
    , _urls :: [String]
    } deriving(Generic, Show)

instance ToJSON Rider
