{-# LANGUAGE OverloadedStrings #-}

module View where

import Model

import Lucid
import qualified Data.Text as T

indexPage :: [Rider] -> Html ()
indexPage riders = do
    doctype_
    html_ $ do
        body_ $ do
            mapM_ mkRider riders

mkRider :: Rider -> Html()
mkRider rider = do
    h1_ $ toHtml $ _title rider
    div_ $ do
        mapM_ mkImgs (_urls rider)

mkImgs :: String -> Html()
mkImgs s = do
    a_ [href_ (T.pack s)] $ do
        img_ [src_ (T.pack s)]
