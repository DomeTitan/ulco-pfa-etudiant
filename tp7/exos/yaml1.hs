{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Yaml

data Person = Person
    { first :: T.Text
    , last :: T.Text
    , birth :: Int
    } deriving (Show)

instance FromJSON Person where 
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> (read <$> v .: "birthyear")

main :: IO ()
main = do
    person <- decodeFileEither "yaml-test1.yaml"
    print (person :: Either ParseException Person)

