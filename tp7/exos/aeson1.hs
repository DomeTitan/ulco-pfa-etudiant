{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import GHC.Generics
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Generic, Show)

instance FromJSON Person

main :: IO ()
main = do
    person <- eitherDecodeFileStrict "aeson-test1.json"
    print (person :: Either String Person)

    person <- eitherDecodeFileStrict "aeson-test2.json"
    print (person :: Either String [Person])

    person <- eitherDecodeFileStrict "aeson-test3.json"
    print (person :: Either String [Person])
