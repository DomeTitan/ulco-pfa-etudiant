import qualified Data.ByteString.Char8 as C
import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import qualified Data.Text.IO as T

main :: IO()
main = do
    file <- C.readFile "text3.hs"
    let contents = E.decodeUtf8 file
    T.putStrLn contents
