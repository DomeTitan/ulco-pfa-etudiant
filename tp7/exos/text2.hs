import qualified Data.Text as T
import qualified Data.Text.IO as T

main :: IO()
main = do
    file <- T.readFile "text2.hs"
    let contents = T.unpack file
    putStrLn contents
