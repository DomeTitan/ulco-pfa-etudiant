{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Show)

instance FromJSON Person where 
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> v .: "birthyear"
        <*> v .: "speakenglish"

main :: IO ()
main = do
    person <- eitherDecodeFileStrict "aeson-test1.json"
    print (person :: Either String Person)

    person <- eitherDecodeFileStrict "aeson-test2.json"
    print (person :: Either String [Person])

    person <- eitherDecodeFileStrict "aeson-test3.json"
    print (person :: Either String [Person])
