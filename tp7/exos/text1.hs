import qualified Data.ByteString.Char8 as C

main :: IO()
main = do
    file <- C.readFile "text1.hs"
    let contents = C.unpack file
    putStrLn contents
