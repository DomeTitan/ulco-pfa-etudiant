{-# LANGUAGE OverloadedStrings #-}

module Movie1 where

import Data.Text (Text)
import Database.SQLite.Simple

instance FromRow Text where
    fromRow = field

dbSelectAllMovies :: Connection -> IO[(Int, Text, Int)]
dbSelectAllMovies conn = query_ conn 
    "SELECT * FROM movie"

dbSelectAllProds :: Connection -> IO[(Text, Int, Text, Text)]
dbSelectAllProds conn = query_ conn 
    "SELECT movie_title, movie_year, person_name, role_name \
    \FROM prod \
    \INNER JOIN movie ON movie_id = prod_movie \
    \INNER JOIN person ON person_id = prod_person \
    \INNER JOIN role ON role_id = prod_role"

dbSelectMoviesFromPersonId :: Connection -> Int -> IO([Text])
dbSelectMoviesFromPersonId conn x= query conn
    "SELECT movie_title \
    \FROM prod \
    \INNER JOIN movie ON movie_id = prod_movie \
    \INNER JOIN person ON person_id = prod_person \
    \WHERE person_id = ?"
    (Only x)