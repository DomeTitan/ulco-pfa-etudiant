{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Text (Text)
import Database.SQLite.Simple
import GHC.Generics (Generic)

data MyData = MyData
    { _id :: Int
    , _name :: Text
    } deriving (Generic, Show)

instance FromRow MyData where
    fromRow = MyData <$> field <*> field

selectAllArtists :: Connection -> IO[MyData]
selectAllArtists conn = query_ conn
    "SELECT * FROM artist"

main :: IO()
main = withConnection "music.db" selectAllArtists >>= mapM_ print