{-# LANGUAGE OverloadedStrings #-}

import Data.Text (Text)
import Database.SQLite.Simple

selectAllArtists :: Connection -> IO[(Text, Text)]
selectAllArtists conn = query_ conn 
    "SELECT artist_name, title_name \
    \FROM title \
    \INNER JOIN artist ON title_artist = artist_id"

main :: IO()
main = do
    conn <- open "music.db"
    res1 <- selectAllArtists conn
    print res1

    res2 <- selectAllArtists conn
    print res2

    close conn
--main = withConnection "music.db" selectAllArtists >>= mapM_ print