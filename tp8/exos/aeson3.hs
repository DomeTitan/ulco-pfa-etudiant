{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import GHC.Generics
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    , address      :: Adresse
    } deriving (Generic, Show)

data Adresse = Adresse
    {
        number      :: Int
        , road      :: T.Text
        , zipcode   :: Int
        , city      :: T.Text
    } deriving (Generic, Show)


instance ToJSON Person
instance ToJSON Adresse

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970 (Adresse 42 "Pont Vieux" 43000 "Espaly")
    , Person "Haskell" "Curry" 1900 (Adresse 1337 "Pere Lachaise" 75000 "Paris")
    ]

main :: IO()
main = do
    encodeFile "out-aeson3.json" persons
