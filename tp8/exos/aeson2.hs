{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

instance ToJSON Person where 
    toJSON (Person f l b) = 
        object [
            "first" .= f
            , "birth" .= b
            , "last" .= l
        ]

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970
    , Person "Haskell" "Curry" 1900
    ]

main :: IO()
main = do
    encodeFile "out-aeson2.json" persons
