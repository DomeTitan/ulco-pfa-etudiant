{-# LANGUAGE OverloadedStrings #-}

import Lucid
import Data.Text.Lazy.IO as L

myHtml :: Html ()
myHtml = do
    h1_ "hello"
    p_ "word"

main :: IO ()
main = L.putStrLn $ renderText myHtml
